<?php

/**
 * Class for Configuration_model
 *
 * @category CategoryName
 * @package  PackageName
 * @author   Chinchu Kurian <chinchu.kurian@fingent.com>
 * @license  PHP License
 * @link     Link
 */

class Home_model extends CI_Model
{
    /**
    * Constructor for Configuration
    */
    function __construct()
    {
        parent::__construct();
        
    }
    // /**
    // * Function to check the location  is currect or not
    // * @param Integer $locationId
    // * @return Array
    // */
    public function getCustomer()
    {
        $result = array();
        $this->db->select('*');
        $this->db->from('registration');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;

    }
     // /**
    // * Function to delete customer
    // * @param Integer $customerId
    // * @return Array
    // */
    public function deleteCustomer($customerId)
    {
        $this->db->where("reg_id", $customerId);
        $this->db->delete("registration");
        return true;

    }
      // /**
    // * Function to delete customer
    // * @param Integer $customerId
    // * @return Array
    // */
    public function changeCustomerStatus($customerId)
    {
        $result = array();
        $this->db->select('status');
        $this->db->from('registration');
        $this->db->where('reg_id', $customerId);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            if($result['status'] == '1')
                $updateStatus = 0;
            else 
                $updateStatus = 1;
             $data = array(
                'status' => $updateStatus );

            $this->db->where('reg_id',$customerId);
            $this->db->update('registration', $data);
            return true;
        }else{
            return false;
        }
        
        


    }
}
?>