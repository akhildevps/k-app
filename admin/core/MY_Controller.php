<?php
/**
 * The base controller which is used by the Front and the Admin controllers
 */
class Base_Controller extends CI_Controller
{
	//global data to show in footer
	public $global_data;

    public function __construct()
    {
        parent::__construct();        
      
    }  

    
}
/* Admin contorller to load Templates */

class Admin_Controller extends Base_Controller
{

	private $template;

	function __construct()
	{ 		parent::__construct();
		
		
    	
	}

	public function template($template_name, $vars = array(), $return = FALSE)
	{  
		$content = $this->load->view('admin/includes/header', $vars, $return);
		$content = $this->load->view('admin/includes/menu', $vars, $return);
		$content = $this->load->view($template_name, $vars, $return);
		$content = $this->load->view('admin/includes/footer', $vars, $return);

		if ($return)
		{
			return $content;
		}
	}
}
?>