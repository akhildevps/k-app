<?php
/**
 * Return assets base url
 * @param  String  $path
 * @return String
**/
function assets_base_url($path = '') {
    $ci =& get_instance(); 
    $conf_path = $ci->config->item('assets_base_url');
    return $conf_path.$path;
}

function stop($var){
    pre($var);
    exit;
}

function pre($var){
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}
?>
