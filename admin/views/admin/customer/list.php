<div class="page">
	<div class="page-main">
		<?php  include('customerHeader.php'); ?>  
		<?php  include('menu.php'); ?>  
		<div class="my-3 my-md-5">
			<div class="container">
				<div class="page-header">
					<div class="col-12">
						<div class="card">
							<div id="accordion" >

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Trigger the modal with a button -->
	<input type="text" id="customer_id" value="" class="hide">
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"></button>

				</div>
				<div class="modal-body">
					<p>Are you sure you want to delete ? </p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
					<button type="button" onclick="deleteCustomer()" class="btn btn-success" data-dismiss="modal">Yes</button>
				</div>

			</div>

		</div>
	</div>


	<script type="text/javascript">

		$(document).ready(function(){
			getCustomerList();

		});

		function getCustomerList()
		{
			var url = base_url+'home/getCustomerList';
			$.ajax({
				url : url,
				type : 'post',          
				data : '',
				success : function(result){

					var result = $.parseJSON(result);
					$("#accordion").html(result);
				}
			}); 
		}
		function deleteCustomerPopup(customerId)
		{
			$('#myModal').modal('show');
			$('#customer_id').val(customerId)

		}
		function deleteCustomer()
		{
			var customerId = $('#customer_id').val();
			var url = base_url+'home/deleteCustomer';
			$.ajax({
				url : url,
				type : 'post',          
				data : 'customerId='+customerId,
				success : function(result){
					var result = $.parseJSON(result);
					if (result.status == 'success') {
						toastr.success(result.msg, 'success',{iconClass:'toast-success'});
						getCustomerList();
					}
					else {
						toastr.error(result.msg, 'failed',{iconClass:'toast-danger'});
					}
				}
			}); 

		}

		function changeCustomerStatus(customerId)
		{
			var url = base_url+'home/changeCustomerStatus';
			$.ajax({
				url : url,
				type : 'post',          
				data : 'customerId='+customerId,
				success : function(result){
					var result = $.parseJSON(result);
					if (result.status == 'success') {
						toastr.success(result.msg, 'success',{iconClass:'toast-success'});
						getCustomerList();
					}
					else {
						toastr.error(result.msg, 'failed',{iconClass:'toast-danger'});
					}
				}
			}); 

		}
	</script>
</body>
</html>