<div class="table-responsive">
								<table class="table table-hover table-outline table-vcenter text-nowrap card-table">
									<thead>
										<tr>
											<th class="text-center w-1"><i class="icon-people"></i></th>
											<th>Name  </th>
											<th>Email  </th>
											<th>Phone No  </th>
											<th class="text-center"><i class="icon-settings"></i></th>
										</tr>

									</thead>
									<tbody class="padding-bottom-20">
										<?php if(!empty($customer)){?>
										<?php foreach ($customer as $key => $value) {
											 $name = isset($value['name']) && !empty($value['name'])?$value['name']:'-';
											 $email_id = isset($value['email_id']) && !empty($value['email_id'])?$value['email_id']:'-';
											 $phone_no = isset($value['phone_no']) && !empty($value['phone_no'])?$value['phone_no']:'-';
											 $status = isset($value['status']) && !empty($value['status'])?$value['status']:'-';
											 $customerId = isset($value['reg_id']) && !empty($value['reg_id'])?$value['reg_id']:'-';
											 if($status ==0){
											 	$activeContent = 'Active';
											 }
											 else{
											 	$activeContent = 'Deactive';
											 }
											?>
										<tr>
											<td class="text-center">
												<div class="avatar d-block" style="background-image: url(<?php echo assets_base_url('assets/images/user_icon.png'); ?>)">
													<?php if($status == 1){ ?><span class="avatar-status bg-green"></span><?php } else{ ?> <span class="avatar-status bg-red"></span><?php } ?>
												</div>
											</td>
											<td>
												<div><?php echo $name; ?></div>
												<!-- <div class="small text-muted">
													Registered: Mar 19, 2018
												</div> -->
											</td>
												<td>
												<div><?php echo $email_id; ?></div>
												<!-- <div class="small text-muted">
													Registered: Mar 19, 2018
												</div> -->
											</td>
												<td>
												<div><?php echo $phone_no; ?></div>
												<!-- <div class="small text-muted">
													Registered: Mar 19, 2018
												</div> -->
											</td>

											<td class="text-right">
												<div class="item-action dropdown">
													<a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>
													<div class="dropdown-menu dropdown-menu-right">
														<a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-tag"></i> Edit </a>
														
														<a href="javascript:void(0)" class="dropdown-item" onClick="changeCustomerStatus('<?php echo $customerId; ?>')"><i class="dropdown-icon fe fe-edit-2" ></i> <?php echo $activeContent; ?> </a>
														

														<div class="dropdown-divider"></div>
														<a href="javascript:void(0)" class="dropdown-item" onClick="deleteCustomerPopup('<?php echo $customerId; ?>')" ><i class="dropdown-icon fe fe-link" ></i> Delete</a>
													</div>
												</div>
											</td>
										</tr>
										<?php } ?>
										<?php }else{ ?>
										<tr>
											<td class="text-center" colspan="5">
												No data
											</td>

											
										</tr>

										<?php }
										?>

									</tbody>
								</table>
							</div>