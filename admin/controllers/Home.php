<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {


	 /**
     * Constructor for Customer 
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
     }
	public function index()
	{
		$data = array();
    	$data= $this->Home_model->getCustomer();
    	$this->template('admin/customer/list',$data);
	}
	public function getCustomerList()
	{
		$data = array();
        $data['customer'] = $this->Home_model->getCustomer();
        $result = $this->load->view('admin/customer/customerListTemplate.php', $data, true);
        echo json_encode($result);
        exit;
	}
 /**
     * Function to delete Customer
     * @param String $page
     * @return Array 
     */
    public function deleteCustomer() {
        $customerId = $this->input->post('customerId');


        if(empty($customerId)) {
            $data['status'] = 'failed';
            $data['msg'] = 'failed';
            echo json_encode($data);
            exit;
        } 
        
        $return = $this->Home_model->deleteCustomer($customerId);
        if ($return) {
                $data['status'] = 'success';
                $data['msg'] ='Deleted';
        
            echo json_encode($data);
            exit;
        } else {
            $data['status'] = 'failed';
            $data['msg'] = 'failed';
            echo json_encode($data);
            exit;
        }
    }
/**
     * Function to change status
     * @return Array 
     */
    public function changeCustomerStatus() {
        $customerId = $this->input->post('customerId');


        if(empty($customerId)) {
            $data['status'] = 'failed';
            $data['msg'] = 'failed';
            echo json_encode($data);
            exit;
        } 
        
        $return = $this->Home_model->changeCustomerStatus($customerId);
        if ($return) {
                $data['status'] = 'success';
                $data['msg'] ='Status changed successfully';
        
            echo json_encode($data);
            exit;
        } else {
            $data['status'] = 'failed';
            $data['msg'] = 'failed';
            echo json_encode($data);
            exit;
        }
    }
}

?>